# -------------------------------------------------
# Project created by QtCreator 2013-06-02T15:13:45
# -------------------------------------------------
TARGET = gvc
TEMPLATE = app

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

FORMS += mainwindow.ui

SOURCES += \
    main.cpp \
    mainwindow.cpp \
    Git/GitEngine.cpp \
    Git/Process.cpp \
    Git/GitBranch.cpp \
    Git/GitBranchModel.cpp
#    Types/GitRepository.cpp \
#    Types/GitBranch.cpp \
#    Types/GitCommit.cpp \
#    Model/GitCommitModel.cpp \
#    Model/GitRepositoryModel.cpp \
#    Model/GitBranchModel.cpp \
#    GitEngine/GitEngine.cpp \
#    GitEngine/GitEngine_p.cpp \
#    Model/GitCommitFilesModel.cpp \
#    Model/GitRepositoryFilesModel.cpp \
#    Types/GitFile.cpp \
#    Types/GitFileTree.cpp


HEADERS += \
    mainwindow.h \
    Git/GitEngine.h \
    Git/Process.h \
    Git/GitBranch.h \
    Git/GitBranchModel.h
#    Helper/Log.h \
#    Types/GitRepository.h \
#    Types/GitBranch.h \
#    Types/GitCommit.h \
#    Model/GitRepositoryModel.h \
#    Model/GitBranchModel.h \
#    Model/GitCommitModel.h \
#    GitEngine/GitEngine_p.h \
#    Model/GitCommitFilesModel.h \
#    Model/GitRepositoryFilesModel.h \
#    Types/GitFile.h \
#    Types/GitFileTree.h
