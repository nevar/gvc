/****************************************************************************
** This file is part of GVC Git Visual Client
** Copyright (C) 2014 Rafal Jastrzebski (mail@nevar.pl)
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 2
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc.,51 Franklin Street,Fifth Floor,Boston,MA 02110-1301, USA.
****************************************************************************/
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "Git/GitEngine.h"
#include <QDir>

static void insertKey( QHash<QString,QString>& hash, const QString& key, const QString& value );
static void stepBack( QHash<QString,QString>& hash, const QString& key );

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    QMainWindow::setCorner( Qt::BottomLeftCorner, Qt::LeftDockWidgetArea );
    ui->setupUi(this);

    connect( &GitEngine::instance(),
             SIGNAL(message(QString)),
             ui->statusBar,
             SLOT(showMessage(QString)) );

    connect( &GitEngine::instance(),
             SIGNAL( s_replyRepositoryList(QList<QString>)),
             this,
             SLOT( reposListReceived(QList<QString>)) );

    ui->repositoryCombo->setModel( &m_repoModel );
    ui->branchesBox->setModel( &m_branchModel );

    emit on_actionSync_repos_triggered();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::changeEvent(QEvent *e)
{
    QMainWindow::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void MainWindow::on_actionSync_repos_triggered()
{
    GitEngine::instance().reqestRepositoryList( QDir::home().absoluteFilePath("repo") );
}

static void insertKey( QHash<QString,QString>& hash, const QString& key, const QString& value )
{
    if ( hash.contains(key) )
    {
        if ( !key.isEmpty() && key[0] != '/' )
        {
            stepBack( hash, key );
            hash.insert( key, value );
            stepBack( hash, key );
        }
    }
    else
    {
        hash.insert( key, value );
    }
}

static void stepBack( QHash<QString,QString>& hash, const QString& key )
{
    QString tmpValueBase = hash[key];
    QString tmpValue = tmpValueBase;
    hash.remove( key );

    tmpValue.remove( tmpValue.count() - key.count(), key.count() );
    if( tmpValue.endsWith('/') )
    {
        tmpValue.remove( tmpValue.count() - 1, 1 );
    }

    QStringList tmpSplitted = tmpValue.split( QDir::separator() );
    if ( tmpSplitted.count() > 0 )
    {
        QString newKey = tmpSplitted.last() + QDir::separator() + key;
        insertKey( hash, newKey, tmpValueBase );
    }
}

void MainWindow::reposListReceived(QList<QString> reposPathList)
{
    QList<QString> currentPathsList = m_repoNameToPath.values();
    qSort( reposPathList );
    qSort( currentPathsList );

    if ( currentPathsList == reposPathList )
    {
        return;
    }

    QList<QString>::iterator i;
    QHash<QString,QString> newRepoNameToPath;
    QList<QString> finalList;

    newRepoNameToPath.clear();
    i = reposPathList.end();
    while( i != reposPathList.begin() )
    {
        i--;
        QString path = *i;
        QString shortName;
        QStringList nameSplitted = path.split( QDir::separator() );
        nameSplitted.removeLast();
        if ( shortName.isNull() )
        {
            shortName = nameSplitted.last();
        }

        insertKey( newRepoNameToPath, shortName, path );
    }

    finalList = newRepoNameToPath.keys();
    m_repoNameToPath = newRepoNameToPath;
    m_repoModel.setStringList( finalList );
}

void MainWindow::on_repositoryCombo_currentIndexChanged(const QString &repoName)
{
    if ( m_repoNameToPath.contains(repoName) )
    {
        m_branchModel.setBranchesList( GitEngine::instance().getBranchesList( m_repoNameToPath[repoName] ) );
    }
}
