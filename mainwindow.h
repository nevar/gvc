/****************************************************************************
** This file is part of GVC Git Visual Client
** Copyright (C) 2014 Rafal Jastrzebski (mail@nevar.pl)
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 2
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc.,51 Franklin Street,Fifth Floor,Boston,MA 02110-1301, USA.
****************************************************************************/
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QStringListModel>

//#include "Model/GitRepositoryModel.h"
//#include "Model/GitBranchModel.h"
//#include "Model/GitCommitModel.h"
//#include "Model/GitRepositoryFilesModel.h"
#include "Git/GitEngine.h"

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT
public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    void changeEvent(QEvent *e);

private slots:
    void on_actionSync_repos_triggered();
    void reposListReceived(QList<QString> reposPathList);

    void on_repositoryCombo_currentIndexChanged(const QString &repoName);

private:
    Ui::MainWindow *ui;

    // List of all repositories
    QStringListModel m_repoModel;

    // map of short repo name to full path
    QHash<QString,QString> m_repoNameToPath;

    // List of all branches on current repo
    GitBranchModel m_branchModel;
};

#endif // MAINWINDOW_H
