/****************************************************************************
** This file is part of GVC Git Visual Client
** Copyright (C) 2014 Rafal Jastrzebski (mail@nevar.pl)
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 2
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc.,51 Franklin Street,Fifth Floor,Boston,MA 02110-1301, USA.
****************************************************************************/

#include "GitBranchModel.h"

GitBranchModel::GitBranchModel(QObject *parent)
    : QAbstractListModel(parent)
{
}

int GitBranchModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)

    return m_branches.count();
}

QVariant GitBranchModel::data(const QModelIndex &index, int role) const
{
    if ( !index.isValid() )
    {
        return QVariant();
    }

    const GitBranch& branch(m_branches.at( index.row() ) );

    QString name;
    if ( !branch.remote().isEmpty() )
    {
        name += "(" + branch.remote() + ")";
    }
    name += " " + m_branches.at( index.row() ).name();

    return QVariant( name );
}

void GitBranchModel::setBranchesList(const QList<GitBranch> &branches)
{
    beginResetModel();
    m_branches = branches;
    endResetModel();
}
