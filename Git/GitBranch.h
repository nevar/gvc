/****************************************************************************
** This file is part of GVC Git Visual Client
** Copyright (C) 2014 Rafal Jastrzebski (mail@nevar.pl)
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 2
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc.,51 Franklin Street,Fifth Floor,Boston,MA 02110-1301, USA.
****************************************************************************/

#ifndef GITBRANCH_H
#define GITBRANCH_H

#include <QDir>
#include <QString>

class GitBranch
{
public:
    GitBranch( const QDir& repoDir, const QString& branchName );

    QString name() const;

    QString fullName() const;

    QDir reoDir() const;

    QString remote() const;

    bool isCurrentBranch() const;

private:
    QString m_name;
    QString m_fullName;
    QDir m_repoDir;
    QString m_remote;
    bool m_isCurrent;
};

#endif // GITBRANCH_H
