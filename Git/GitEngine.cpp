/****************************************************************************
** This file is part of GVC Git Visual Client
** Copyright (C) 2014 Rafal Jastrzebski (mail@nevar.pl)
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 2
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc.,51 Franklin Street,Fifth Floor,Boston,MA 02110-1301, USA.
****************************************************************************/
#include "GitEngine.h"
#include <QDir>

QString GitEngine::cmd_ls("/bin/ls");
QString GitEngine::cmd_find("/usr/bin/find");
QString GitEngine::cmd_git("/usr/bin/git");

GitEngine::GitEngine()
    : QObject(0)
    , m_pProcess( NULL )
{
}

void GitEngine::r_finishedSearchingRepository(Process * pProcess, int exitCode, QProcess::ExitStatus status)
{
    Q_UNUSED( exitCode )
    Q_UNUSED( status )

    if ( !pProcess )
    {
        return;
    }

    QString output( pProcess->readAll() );
    m_pProcess = NULL;
    delete pProcess;

    QList<QString> list( output.split("\n") );
    list.removeAll("");

    QList<QString>::iterator i(list.begin());
    for (; i != list.end(); i++ )
    {
        *i = (*i).trimmed();
    }

    emit s_replyRepositoryList( list );
    QString strCount = QString::number( list.count() );
    emit message( QString("Found %1 repositories.").arg(strCount) );
}

GitEngine &GitEngine::instance()
{
    static GitEngine inst;
    return inst;
}

QList<GitBranch> GitEngine::getBranchesList(const QString &repoPath)
{
    QDir repoDir( repoPath );

    Process process;
    process.setWorkingDirectory( repoDir.absolutePath() );

    QStringList arguments;
    arguments << "branch" << "-a";

    emit message( QString("Scanning for branches from repository: ") + repoDir.absolutePath() );
    process.start( cmd_git, arguments );
    process.waitForFinished();

    QString output( process.readAll() );
    QStringList list = output.split("\n");
    list.removeAll("");

    foreach (QString branchName, list) {
        if( branchName.contains("HEAD -> ") )
        {
            list.removeAll( branchName );
            break;
        }
    }

    emit message( QString("Found branches: ") + QString::number( list.count() ) );

    QList<GitBranch> branches;
    foreach (QString branchName, list) {
        branches.append( GitBranch( repoPath, branchName ) );
    }

    return branches;

}

void GitEngine::reqestRepositoryList(const QString &searchPath)
{
    if ( m_pProcess )
    {
        emit message( "[Warning] Another process is already running." );
        return;
    }

    m_pProcess = new Process(this);

    connect( m_pProcess, SIGNAL(s_finished(Process*,int,QProcess::ExitStatus)),
             this, SLOT(r_finishedSearchingRepository(Process*,int,QProcess::ExitStatus)) );
    QStringList arguments;

    arguments << searchPath << "-iname" << ".git";
    emit message( QString("Searching repositories in directory: ") + searchPath );
    m_pProcess->start( cmd_find, arguments );
}
