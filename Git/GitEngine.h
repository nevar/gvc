/****************************************************************************
** This file is part of GVC Git Visual Client
** Copyright (C) 2014 Rafal Jastrzebski (mail@nevar.pl)
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 2
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc.,51 Franklin Street,Fifth Floor,Boston,MA 02110-1301, USA.
****************************************************************************/
#ifndef GITENGINE_H
#define GITENGINE_H

#include "Git/Process.h"
#include "Git/GitBranch.h"
#include "Git/GitBranchModel.h"
#include <QObject>
#include <QStringList>

class GitEngine : public QObject
{
    Q_OBJECT
public:
    /**
     * @brief instance returns instance of GitEngine singleton object.
     * @return instance of GitEngine.
     */
    static GitEngine& instance();

    /**
     * @brief getBranchesList Get list branches on repository.
     * @param repoPath Path to repository .git folder.
     * @return List of branch name strings.
     */
    QList<GitBranch> getBranchesList( const QString& repoPath );

public slots:
    /**
    * @brief r_reqestRepositoryList Sends request to receive repository list.
    * @param searchPath Base path from where to search repositories. Repositories
    * are searched recursively down starting from "searchPath" path.
    * Response will be emited by s_replyRepositoryList signal.
    */
    void reqestRepositoryList(const QString & searchPath);

signals:
    /**
     * @brief s_replyRepositoryList Returns found list of repositories.
     * @param repos Repository list.
     */
    void s_replyRepositoryList( const QList<QString> repos );

    /**
     * @brief message emits system information.
     */
    void message( const QString) const;

private slots:
    void r_finishedSearchingRepository(Process * pProcess, int exitCode, QProcess::ExitStatus status);

private:
    // Singleton class
    explicit GitEngine();
    GitEngine(const GitEngine&) : QObject(0) {}
    const GitEngine& operator=( const GitEngine& ) const { return *this; }
    ~GitEngine() {}

private:
    Process* m_pProcess; // currently executing process

private:
    static QString cmd_ls;
    static QString cmd_find;
    static QString cmd_git;
};

#endif // GITENGINE_H
