/****************************************************************************
** This file is part of GVC Git Visual Client
** Copyright (C) 2014 Rafal Jastrzebski (mail@nevar.pl)
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 2
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc.,51 Franklin Street,Fifth Floor,Boston,MA 02110-1301, USA.
****************************************************************************/

#include "GitBranch.h"

GitBranch::GitBranch(const QDir &repoDir, const QString &branchName)
{
    m_fullName = branchName;
    m_repoDir = repoDir;

    if( branchName.startsWith("* ") )
    {
        m_isCurrent = true;
        m_fullName.remove(0, 2);
    }

    m_fullName = m_fullName.trimmed();
    m_name = m_fullName;
    if ( m_fullName.startsWith("remotes/"))
    {
        QStringList list = m_fullName.split("/");
        m_remote = list[1];
        m_name = list[2];
    }

}

QString GitBranch::name() const
{
    return m_name;
}

QString GitBranch::fullName() const
{
    return m_fullName;
}

QDir GitBranch::reoDir() const
{
    return m_repoDir;
}

QString GitBranch::remote() const
{
    return m_remote;
}

bool GitBranch::isCurrentBranch() const
{
    return m_isCurrent;
}
